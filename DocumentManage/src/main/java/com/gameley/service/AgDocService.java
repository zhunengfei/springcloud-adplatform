package com.gameley.service;

import com.gameley.entity.AgDoc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-30 14:46:58
 */
public interface AgDocService extends  IService<AgDoc> {
	
}
